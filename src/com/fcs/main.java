package com.fcs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class main {

	public static void main(String[] args) throws NumberFormatException, IOException  {

		ServerSocket server = new ServerSocket(Integer.parseInt(args[1]));
		while (true) {
			try (Socket socket = server.accept()) {
				try (BufferedReader br = new BufferedReader(new FileReader(args[2]))) {
					StringBuilder sb = new StringBuilder();
					String line = br.readLine();

					while (line != null) {
						sb.append(line);
						sb.append(System.lineSeparator());
						line = br.readLine();
					}
					
					String content = sb.toString();
					String httpResponse = "HTTP/1.1 200 OK\r\n\r\n" + content;
					socket.getOutputStream().write(httpResponse.getBytes("UTF-8"));
				} catch (IOException e) {
					String httpResponse = "HTTP/1.1 404 Not Found my dude\r\n";
					socket.getOutputStream().write(httpResponse.getBytes("UTF-8"));
				}
            }
            catch (IOException e) {
                System.err.println("Cannot accept connection");
                return;
            }
			
		}
	}
	
}
